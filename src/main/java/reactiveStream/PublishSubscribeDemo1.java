package reactiveStream;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

public class PublishSubscribeDemo1 {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(1);
        try (
            //Creazione del publisher
            SubmissionPublisher<Mail> publisher =
                    new SubmissionPublisher<>(executor, Flow.defaultBufferSize())) {
           
        	//Creazione e registrazione del subscriber
            MailSubscriber subscriber = new MailSubscriber();
            publisher.subscribe(subscriber);
            
            Mail[] mailData = new Mail[]{
                new Mail("mail1@mail.it", "Testo 1"),
                new Mail("mail2@mail.it", "Testo 2")
            };
            
            //Pubblicazione lista mail
            Arrays.asList(mailData).stream().forEach(mail -> publisher.submit(mail));
            
            executor.shutdown();
        }
    }
}
