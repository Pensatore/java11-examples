package reactiveStream;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;

public class MailSubscriber implements Subscriber<Mail>{
    private Subscription subscription;
    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
    }
    @Override
    public void onNext(Mail mail) {
        System.out.println("Indirizzo:" + mail.getAddress() + " Testo:" + mail.getMessage());
        subscription.request(1);
    }
    @Override
    public void onError(Throwable throwable) {
        throwable.printStackTrace();
    }
    @Override
    public void onComplete() {
        System.out.println("Processo mail completato");
    }
}