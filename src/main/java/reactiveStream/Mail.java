package reactiveStream;

public class Mail {
    private String address;
    private String message;
    
    public Mail(String address, String message) {
      this.setAddress(address);
      this.setMessage(message);
    }

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}