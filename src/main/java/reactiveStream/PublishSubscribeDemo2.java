package reactiveStream;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

public class PublishSubscribeDemo2 {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor1 = Executors.newFixedThreadPool(1);
        ExecutorService executor2 = Executors.newFixedThreadPool(1);
        
        //Creazione e registrazione di un processore;
        try ( //Creazione di un publisher
                SubmissionPublisher<Mail> publisher = new SubmissionPublisher<>(executor1, Flow.defaultBufferSize())) {
        	
            //Creazione e registrazione di un processore;
            MailProcessor mailProcessor = new MailProcessor(
                    mail -> {
                        mail.setMessage(mail.getMessage() + "[Firma per indirizzo:" + mail.getAddress()+"]");
                        return mail;
                    }, executor2, Flow.defaultBufferSize());
            
            publisher.subscribe(mailProcessor);
            
            //Creazione e registrazione di un subscriber
            MailSubscriber mailSubscriber = new MailSubscriber();
            mailProcessor.subscribe(mailSubscriber);
            
            Mail[] mailData = new Mail[]{
                new Mail("mail1@mail.it", "Testo mail 1"),
                new Mail("mail2@mail.it", "Testo mail 2")
            };
            
            //Pubblicazione lista mail
            Arrays.asList(mailData).stream().forEach(mail -> publisher.submit(mail));
        }
        
        executor1.shutdown();
    }
}