package reactiveStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Flow.Processor;
import java.util.concurrent.Flow.Subscription;
import java.util.concurrent.SubmissionPublisher;
import java.util.function.Function;

public class MailProcessor extends SubmissionPublisher<Mail> implements Processor<Mail,Mail> {
  private final Function<Mail,Mail> function;
  private Subscription subscription;
  private final ExecutorService executor;
  
  public MailProcessor(Function<Mail,Mail> function, ExecutorService executor, int maxBufferCapacity) {
    super(executor,maxBufferCapacity);
    this.function = function;
    this.executor = executor;
  }
  
  @Override
  public void onSubscribe(Subscription subscription) {
    this.subscription = subscription;
    subscription.request(1);
  }
  @Override
  public void onNext(Mail mail) {
    submit(function.apply(mail));
    subscription.request(1);
  }
  @Override
  public void onError(Throwable t) {
    t.printStackTrace();
  }
  @Override
  public void onComplete() {
    System.out.println("Processo flusso mail completato");
    close();
    executor.shutdown();
  }
}