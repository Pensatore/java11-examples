/**
 * 
 */
/**
 * @author Ezio
 *
 */
module java11Examples {
	exports io.github.biezhi.java11.string;
	exports prove;
	exports io.github.biezhi.java11.singlefile;
	exports io.github.biezhi.java11.interfaces;
	exports io.github.biezhi.java11.processor;
	exports reactiveStream;
	exports io.github.biezhi.java11.files;
	exports io.github.biezhi.java11.time;
	exports io.github.biezhi.java11.collections;
	exports io.github.biezhi.java11.trywithresources;
	exports io.github.biezhi.java11.http;
	exports io.github.biezhi.java11.var;

	requires gson;
	requires java.net.http;
}