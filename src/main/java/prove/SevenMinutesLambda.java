package prove;

import java.util.*;

public class SevenMinutesLambda {

	public static void main(String[] args) {

		// List strings = Arrays.asList("Lambda ", "expressions ", "are ", "cool");
		List<String> strings = List.of("Lambda ", "expressions ", "are ", "cool");

		strings.parallelStream().filter(s -> {
			System.out.format("filter:  %s [%s]\n", s, Thread.currentThread().getName());
			return true;
		}).map(s -> {
			System.out.format("map:     %s [%s]\n", s, Thread.currentThread().getName());
			return s.toUpperCase();
		}).sorted((s1, s2) -> {
			System.out.format("sort:    %s <> %s [%s]\n", s1, s2, Thread.currentThread().getName());
			return s1.compareTo(s2);
		}).forEach(s -> System.out.format("forEach: %s [%s]\n", s, Thread.currentThread().getName()));
	}

}