package prove;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntFunction;

public class EsempioVar {

	public static void main(String[] args) {
		var numbers = new int[] { 1, 2, 3, 4, 5, 6, 7 };
		int[] subset = Arrays.stream(numbers).filter( a -> (a < 3 || a > 5) ).toArray();

		for (int i = 0; i < subset.length; i++) {
			System.out.println(subset[i]);
		}
		
		System.out.print("\n------------------------------------------------------------\n");
		var numbers2 = List.of("a", "b", "c");
		for (var nr : numbers2) {
		   System.out.println(nr + " ");
		}

		System.out.print("\n------------------------------------------------------------\n");
		IntFunction<Integer> doubleIt2 = (@Valid final var x) -> x * 2;   // Will compile from Java 11
		System.out.println("\n" + doubleIt2.apply(5) + " ");
		
	}
}
