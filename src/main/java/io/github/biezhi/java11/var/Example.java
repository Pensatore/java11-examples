package io.github.biezhi.java11.var;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * æ„Ÿå�— JEP 286 çš„é­”æ³•ä¸–ç•Œ
 *
 * @author biezhi
 * @date 2018/7/10
 */
public class Example {

    public static void main(String[] args) throws Exception {
 //       var list   = new ArrayList<String>();   // è‡ªåŠ¨æŽ¨æ–­ ArrayList<String>
 //       var stream = list.stream();             // è‡ªåŠ¨æŽ¨æ–­ Stream<String>

        var newList = List.of("hello", "biezhi");
        newList.forEach(System.out::println);

        String fileName = "./pom.xml";

        var path  = Paths.get(fileName);
        var bytes = Files.readAllBytes(path);

        System.out.println("å­—èŠ‚æ•°ç»„: " + bytes);

        for (var b : bytes) {
        	System.out.println("var b: " + b);
        }

        try (var foo = new FileInputStream(new File(""))) {
            System.out.println(foo);
        } catch (Exception e) {
            // ignore
        }

    }
}
