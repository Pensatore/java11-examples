package io.github.biezhi.java11.collections;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Map.entry;

/**
 * @author biezhi
 * @date 2018/7/10
 */
public class Example {

	public static void main(String[] args) {

		List<String> initList = List.of("biezhi", "github", "Prove");
		System.out.println("List.of: " + initList);
		
		Set<String> initSet = Set.of("aˆ‘", "aa", "bb", "cc", "vv");
		System.out.println("Set.of: " + initSet);

		Map<Integer, String> initLMap = Map.of( 
				2017, "biezhi", 
				2018, "biezhi...");
		System.out.println("Map.of: " + initLMap);
		
		Map<Integer, String> initMapEntry = Map.ofEntries(
				entry(20, "20"), 
				entry(30, "30"), 
				entry(40, "40"));

		System.out.println("Map.ofEntries: " + initMapEntry);
		
		// Map.Entry
		Map.Entry<String, String> immutableMapEntry = Map.entry("biezhi", "emmmm");
		Map<String, String> initImmutableMapEntry = Map.ofEntries(immutableMapEntry);
		System.out.println("Map.immutableMapEntry: " + initImmutableMapEntry);
		
		try {
			initImmutableMapEntry.remove("emmmm") ;
		} catch (UnsupportedOperationException e) {
			System.out.println("Map.immutableMapEntry non può essere modificata... " + e.fillInStackTrace());			
		}
		
	}

}
