package io.github.biezhi.java11.collections;

/**
 * Java7 å¼•å…¥äº†ä¸€ä¸ªæ–°çš„ç‰¹æ€§ï¼šDiamond Operatorï¼Œæ�¥é�¿å…�å†—é•¿ä»£ç �å’Œæ��å�‡å�¯è¯»æ€§ã€‚
 * <p>
 * ç„¶è€Œåœ¨ Java 8 ä¸­ï¼ŒOracle å�‘çŽ°åœ¨ Diamond æ“�ä½œå™¨å’ŒåŒ¿å��å†…éƒ¨ç±»çš„ä½¿ç”¨ä¸­å­˜åœ¨ä¸€äº›å±€é™�æ€§ï¼Œ
 * å�Žæ�¥ä¿®å¤�äº†è¿™äº›é—®é¢˜å¹¶å‡†å¤‡å°†å…¶ä½œä¸º Java9 çš„ä¸€éƒ¨åˆ†å�‘å¸ƒå‡ºåŽ»ã€‚
 *
 * @author biezhi
 * @date 2018/7/10
 */
public class DiamondOperatorExample {

    static abstract class MyHandler<T> {

        private T content;

        public MyHandler(T content) {
            this.content = content;
            System.out.println("æž„é€ å‡½æ•°æ”¶åˆ°äº†ä¸€æ�¡å†…å®¹: " + content.toString());
        }

        public T getContent() {
            return content;
        }

        public void setContent(T content) {
            this.content = content;
        }

        abstract void handle();
    }

    public static void main(String... args) {
        MyHandler<Integer> intHandler = new MyHandler<>(1) {
            @Override
            public void handle() {
                System.out.println("æ”¶åˆ°çº¢åŒ… > " + getContent() + "å…ƒ");
            }
        };
        intHandler.handle();

        System.out.println("===================ç¥žå¥‡çš„åˆ†å‰²çº¿===================");

        MyHandler<? extends Integer> intHandler1 = new MyHandler<>(10) {
            @Override
            void handle() {
                System.out.println("æ”¶åˆ°çº¢åŒ… > " + getContent() + "å…ƒ");
            }
        };
        intHandler1.handle();

        System.out.println("===================ç¥žå¥‡çš„åˆ†å‰²çº¿===================");

        MyHandler<?> handler = new MyHandler<>("é­”æ³•å¸ˆ") {
            @Override
            void handle() {
                System.out.println("é©¬ä¸ŠæŠŠ [" + getContent() + "] ç»™å¤„ç�†äº†");
            }
        };
        handler.handle();

    }

}
